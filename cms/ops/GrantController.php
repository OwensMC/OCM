<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/cms/app/lib/Grant.php';

class GrantController
{
    private $verb;
    private $grant_id;
    private $user_id;
    private $read_all;
    private $offices;

    private $grant;

    public function __construct($verb, $grant_id, $auth_row)
    {
       $this->grant_id = $grant_id;
       $this->verb = $verb;
       $this->user_id = $auth_row['user_id'];
       $this->read_all = $auth_row['read_all'];
       $this->offices = $auth_row['read_office'];

       $this->grant = new Grant();
    }

    public function process()
    {
        switch ($this->verb)
        {
            case 'GET':
                if ($this->grant_id)
                    $response = $this->getSingle($this->grant_id);
                else 
                    $response = $this->getAll($this->read_all, $this->offices);
                break;
            case 'POST':
                $response = $this->create($this->user_id);
                break;
            case 'PUT':
                $response = $this->update($this->grant_id);
                break;
            case 'DELETE':
                $response = $this->delete($this->grant_id);
                break;
            default:
                $response = $this->notFound();
                break;
        }
        header($response['status_code_header']);
        if ($response['body'])
            echo $response['body'];
    }

    public function getAll($read_all, $offices)
    {
        $result = $this->grant->getAllGrants($read_all, $offices);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    public function getSingle($grant_id)
    {
        $result = $this->grant->getSingleGrant($grant_id);
        if (!$result)
            return $this->notFound();
        if ($this->read_all || in_array($result['office_id'], $this->offices))
        {
            $response['status_code_header'] = 'HTTP/1.1 200 OK';
            $response['body'] = json_encode($result);
        }
        else
        {
            $response['status_code_header'] = 'HTTP/1.1 403 Forbidden';
            $response['body'] = null;
        }
        
        return $response;
    }

    public function create($user_id)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (!$this->validate($input))
            return $this->garbage();
        $this->grant->createGrant($user_id, $input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    public function update($grant_id)
    {
        $result = $this->grant->getSingleGrant($grant_id);
        
        if (!$result)
            return $this->notFound();
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (!$this->validate($input))
            return $this->garbage();
        $this->grant->editGrant($grant_id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    public function delete($grant_id)
    {
        $result = $this->grant->getSingleGrant($grant_id);
        if (!$result)
            return $this->notFound();
        $this->grant->deleteGrant($grant_id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    public function validate($input)
    {
        if (!isset($input['office_id']))
            return false;
        if (!isset($input['funding_code']))
            return  false;
        if (!isset($input['source']))
            return  false;
        if (!isset($input['is_active']))
            return  false;
        //if (!isset($input['description']))
            //return  false;
        //if (!isset($input['running_total']))
            //return  false;
        if (!isset($input['start_date']))
            return  false;
        //if (!isset($input['end_date']))
            //return  false;
        //if (!isset($input['unit_rate']))
            //return  false;
        //if (!isset($input['billing_type']))
            //return  false;
        
        return true;
    }

    public function garbage()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    public function notFound()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
