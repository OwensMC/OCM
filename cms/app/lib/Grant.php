<?php

class Grant
{
    public function __construct()
    {
        
    }

    public function createGrant($user_id, $data)
    {
        /*input validation for required fields*/
        $office_id = $funding_code = $source = $is_active = $hours = $start_date = $end_date = "";
        $officeErr = $fundingErr = $sourceErr = $activeErr = $totalErr = $dateErr = "";

        if(!empty(DB::escapeString($data['office_id']))){
            $office_id = DB::escapeString($data['office_id']);
        }
        else {
            $officeErr = "Please provide an office id";
        }

        if(!empty(DB::escapeString($data['funding_code']))){
          $funding_code = DB::escapeString($data['funding_code']);
        }else {
            $fundingErr = "Please provide a funding code"; 
        }

        if(!empty(DB::escapeString($data['source']))){
            $source = DB::escapeString($data['source']);
        }else {
            $sourceErr = "Please provide a source"; 
        }

        if(!empty(DB::escapeString($data['is_active']))){
            $is_active = DB::escapeString($data['is_active']);
        }else {
            $activeErr = "Please mark whether this grant is active"; 
        }

        if(!empty(DB::escapeString($data['start_date']))){
            $start_date = DB::escapeString($data['start_date']);
        }else {
            $dateErr = "Please provide a start date"; 
        }

        $description = DB::escapeString($data['description']);        
        $end_date = DB::escapeString($data['end_date']);

        $sql = "INSERT INTO grants (
                    office_id, 
                    user_id, 
                    funding_code, 
                    source, 
                    is_active, 
                    description, 
                    start_date, 
                    end_date
                    ) 
                VALUES (
                    (SELECT value FROM menu_office WHERE menu_order = ('$office_id')), 
                    ('$user_id'), 
                    ('$funding_code'), 
                    ('$source'), 
                    ('$is_active'), 
                    ('$description'), 
                    ('$start_date'), 
                    ('$end_date'))";
        $result = DB::query($sql) or trigger_error('Could not create grant.');
        return DBResult::numRows($result);
    }

    // Get all grants that user has permission to 
    // view according to their security level (group).
    // This function expects a boolean and an array of
    // office ids like so:
    // offices[M, P, S, ...]
    // Basically, pass in $auth_row['read_all'] and
    // $auth_row['read_office']
    public function getAllGrants($read_all, $offices)
    {
        $sql = "SELECT * FROM grants";
        $result = DB::query($sql) or trigger_error('Could not get grants.');

        $grants = array();

        while ($row = DBResult::fetchRow($result))
        {
            // Check if user has permission to read all files (in system)
            // or if user has permission to read from grant office
            if ($read_all || in_array($row['office_id'], $offices))
            {
                extract($row);
                $grant = array(
                    "grant_id" =>           $grant_id,
					"office_id" =>          $office_id,
					"user_id" =>            $user_id,
					"funding_code" =>       $funding_code,
					"source" =>             $source,
					"description" =>        $description,
					"hours" =>              $hours,
					"start_date" =>         $start_date,
					"end_date" =>           $end_date,
					"is_active" =>          $is_active,
					"unit_rate" =>          $unit_rate,
					"gender_restriction" => $gender_restriction,
					"grant_problem" =>      $grant_problem,
					"grant_sp_problem" =>   $grant_sp_problem,
					"county_restriction" => $county_restriction,
					"grant_county" =>       $grant_county,
					"g_ethnicity" =>        $g_ethnicity,
					"veteran_grant" =>      $veteran_grant,
					"case_restricted" =>    $case_restricted,
					"poverty_restricted" => $poverty_restricted,
					"grant_color" =>        $grant_color
                );
                array_push($grants, $grant);
            }
        }

        return $grants;
    }

    public function getSingleGrant($grant_id)
    {
        $sql = "SELECT      grants.*, menu_office.label AS office_name,
                            menu_gender.label AS gender, menu_problem.label AS problem, menu_sp_problem.label AS sp_problem, menu_ethnicity.label AS ethnicity
                FROM        grants
                INNER JOIN  menu_office ON grants.office_id = menu_office.value
                LEFT JOIN   menu_gender ON grants.gender_restriction = menu_gender.value
                LEFT JOIN   menu_problem ON grants.grant_problem = menu_problem.value
                LEFT JOIN   menu_sp_problem ON grants.grant_sp_problem = menu_sp_problem.value
                LEFT JOIN   menu_ethnicity ON grants.g_ethnicity = menu_ethnicity.value
                WHERE       grant_id = ('$grant_id')";
        $result = DB::query($sql) or trigger_error('Could not find grant.');
        $row = DBResult::fetchRow($result);

        return $row;
    }

    // Delete grant AND corresponding transactions belonging to grant
    public function deleteGrant($grant_id)
    {
        $sql = "DELETE transactions FROM transactions WHERE grant_id = ('$grant_id')";

        DB::query("BEGIN");

        $result1 = DB::query($sql);

        $sql = "DELETE grants FROM grants WHERE grant_id = ('$grant_id')";

        $result2 = DB::query($sql);

        if ($result1 && $result2)
        {
            DB::query("COMMIT");
        }
        else
        {
            DB::query("ROLLBACK");
            return 0;
        }

        return 1;
    }

    public function restrictGrant($grant_id, $data)
    {
        $gender_restriction = DB::escapeString($data['gender_restriction']);
        $grant_problem = DB::escapeString($data['grant_problem']);
        $grant_sp_problem = DB::escapeString($data['grant_sp_problem']);
        $county_restriction = DB::escapeString($data['county_restriction']);
        $grant_county = DB::escapeString($data['grant_county']);
        $g_ethnicity = DB::escapeString($data['g_ethnicity']);
        $veteran_grant = DB::escapeString($data['veteran_grant']);
        $case_restricted = DB::escapeString($data['case_restricted']);
        $poverty_restricted = DB::escapeString($data['poverty_restricted']);

        $grant_id = DB::escapeString($grant_id);

        /*
        $sql = "UPDATE  grants 
                SET     gender_restriction = (SELECT value FROM menu_gender WHERE menu_order = ('$gender_restriction')), 
                        grant_problem = (SELECT value FROM menu_problem WHERE menu_order = ('$grant_problem')),
                        grant_sp_problem = (SELECT value FROM menu_sp_problem WHERE menu_order = ('$grant_sp_problem')),
                        county_restriction = ('$county_restriction'),
                        grant_county = ('$grant_county'),
                        g_ethnicity = (SELECT value FROM menu_ethnicity WHERE menu_order = ('$g_ethnicity')),
                        veteran_grant = ('$veteran_grant'),
                        case_restricted = ('$case_restricted'),
                        poverty_restricted = ('$poverty_restricted') 
                WHERE   grant_id = ('$grant_id')";
        */

        $sql = "UPDATE  grants 
                SET     gender_restriction = (SELECT value FROM menu_gender WHERE menu_order = ('$gender_restriction')), 
                        grant_problem = (SELECT value FROM menu_problem WHERE menu_order = ('$grant_problem')),
                        grant_sp_problem = (SELECT value FROM menu_sp_problem WHERE menu_order = ('$grant_sp_problem')),
                        county_restriction = ('$county_restriction'),
                        grant_county = ('$grant_county'),
                        g_ethnicity = (SELECT value FROM menu_ethnicity WHERE menu_order = ('$g_ethnicity'))";
        
        if ($veteran_grant != -1)
        {
            $sql .= ", veteran_grant = ('$veteran_grant')";
        }
        else
        {
            $sql .= ", veteran_grant = (NULL)";
        }

        if ($case_restricted != NULL)
        {
            $sql .= ", case_restricted = ('$case_restricted')";
        }
        else
        {
            $sql .= ", case_restricted = (NULL)";
        }

        if (!empty($poverty_restricted))
        {
            $sql .= ", poverty_restricted = ('$poverty_restricted')";
        }
        else
        {
            $sql .= ", poverty_restricted = (NULL)";
        }

        $sql .= "WHERE grant_id = ('$grant_id')";

        $result = DB::query($sql) or trigger_error('Could not add grant restrictions.');
        return DB::affectedRows($result);
    }

    public function editGrant($grant_id, $data)
    {
        $office_id = $funding_code = $source = $is_active = $hours = $start_date = $end_date = "";
        $officeErr = $fundingErr = $sourceErr = $activeErr = $totalErr = $dateErr = "";

        if(!empty(DB::escapeString($data['office_id']))){
            $office_id = DB::escapeString($data['office_id']);
        }
        else {
            $officeErr = "Please provide an office id";
        }

        if(!empty(DB::escapeString($data['funding_code']))){
          $funding_code = DB::escapeString($data['funding_code']);
        }else {
            $fundingErr = "Please provide a funding code"; 
        }

        if(!empty(DB::escapeString($data['source']))){
            $source = DB::escapeString($data['source']);
        }else {
            $sourceErr = "Please provide a source"; 
        }

        if(!empty(DB::escapeString($data['is_active']))){
            $is_active = DB::escapeString($data['is_active']);
        }else {
            $activeErr = "Please mark whether this grant is active"; 
        }
        
        if(!empty(DB::escapeString($data['hours']))){
            $hours = DB::escapeString($data['hours']);
        }else {
            $totalErr = "Please provide an amount"; 
        }

        if(!empty(DB::escapeString($data['start_date']))){
            $start_date = DB::escapeString($data['start_date']);
        }else {
            $dateErr = "Please provide a start date"; 
        }

        $description = DB::escapeString($data['description']);        
        $end_date = DB::escapeString($data['end_date']);
        $grant_id = DB::escapeString($grant_id);
        
        $sql = "UPDATE  grants
                SET     office_id = (SELECT value FROM menu_office WHERE menu_order = ('$office_id')), source = ('$source'), funding_code = ('$funding_code'), description = ('$description'), is_active = ('$is_active'),     start_date = ('$start_date'), end_date = ('$end_date')
                WHERE   grant_id = ('$grant_id')";
        $result = DB::query($sql) or trigger_error('Could not edit grant.');

        // need to store result to get the right num of rows affected?
        return DB::affectedRows($result);
    }
    
    function deposit($user_id, $grant_id, $data)
    {
        $new_hours = DB::escapeString($data['new_hours']);
        $grant_id = DB::escapeString($grant_id);

        $sql = "UPDATE  grants 
                SET     hours = ('$new_hours')
                WHERE   grant_id = ('$grant_id')";

        DB::query("BEGIN");

        $result1 = DB::query($sql);

        if(!empty(DB::escapeString($data['date_of_service']))){

            $date_of_service = DB::escapeString($data['date_of_service']);

        }

        if(!empty(DB::escapeString($data['time_spent']))){

            $time_spent = DB::escapeString($data['time_spent']);

        }

        $notes = DB::escapeString($data['notes']);

        $sql = "INSERT INTO transactions (
                    user_id,
                    grant_id,
                    date_of_service,
                    time_spent,
                    notes)
                VALUES (
                    ('$user_id'),
                    ('$grant_id'),
                    ('$date_of_service'),
                    ('$time_spent'),
                    ('$notes'))";

        $result2 = DB::query($sql);

        if ($result1 && $result2)
        {
            DB::query("COMMIT");
        }
        else
        {
            DB::query("ROLLBACK");
            return false;
        }

        return true;
    }

    function bill($user_id, $grant_id, $data)
    {
        $new_hours = DB::escapeString($data['new_hours']);
        $grant_id = DB::escapeString($grant_id);

        $sql = "UPDATE  grants 
                SET     hours = ('$new_hours')
                WHERE   grant_id = ('$grant_id')";

        DB::query("BEGIN");

        $result1 = DB::query($sql);

        if(!empty(DB::escapeString($data['case_id']))){

            $case_id = DB::escapeString($data['case_id']);

        }

        if(!empty(DB::escapeString($data['caseworker_id']))){

            $caseworker_id = DB::escapeString($data['caseworker_id']);

        }

        if(!empty(DB::escapeString($data['date_of_service']))){

            $date_of_service = DB::escapeString($data['date_of_service']);

        }

        if(!empty(DB::escapeString($data['time_spent']))){

            $time_spent = DB::escapeString($data['time_spent']);

        }
        
        $menu_category = DB::escapeString($data['menu_category']);
        $notes = DB::escapeString($data['notes']);

        $sql = "INSERT INTO transactions (
                    user_id,
                    grant_id,
                    case_id,
                    caseworker_id,
                    menu_category,
                    date_of_service,
                    time_spent,
                    notes)
                VALUES (
                    ('$user_id'),
                    ('$grant_id'),
                    ('$case_id'),
                    ('$caseworker_id'),
                    (SELECT value FROM menu_category WHERE menu_order = ('$menu_category')),
                    ('$date_of_service'),
                    ('$time_spent'),
                    ('$notes'))";

        $result2 = DB::query($sql);

        if ($result1 && $result2)
        {
            DB::query("COMMIT");
        }
        else
        {
            DB::query("ROLLBACK");
            return false;
        }

        return true;
    }

    public function getAllBillingTypes()
    {
        $sql = "SELECT * FROM billing_types";
        $result = DB::query($sql);

        $billing_types = array();

        while ($row = DBResult::fetchRow($result))
        {
            extract($row);
            $billing_type = array(
                "type_id" => $type_id,
                "name" => $name
            );
            array_push($billing_types, $billing_type);
        }

        return $billing_types;
    }

    public function getAllOffices()
    {
        $sql = "SELECT * FROM menu_office";
        $result = DB::query($sql);

        $offices = array();

        while ($row = DBResult::fetchRow($result))
        {
            extract($row);
            $office = array(
                "value" =>      $value,
                "label" =>      $label,
                "menu_order" => $menu_order
            );
            array_push($offices, $office);
        }

        return $offices;
    }

    public function getAllCases()
    {
        $sql = "SELECT * FROM cases";
        $result = DB::query($sql);

        $cases = array();

        while ($row = DBResult::fetchRow($result))
        {
            extract($row);
            $case = array(
                "case_id" => $case_id,
                "number" => $number
            );
            array_push($cases, $case);
        }

        return $cases;
    }

    public function getAllUsers()
    {
        $sql = "SELECT * FROM users";
        $result = DB::query($sql);
        $users = array();
        while ($row = DBResult::fetchRow($result))
        {
            extract($row);
            $user = array(
                "user_id" => $user_id,
                "first_name" => $first_name,
                "last_name" => $last_name
            );
            array_push($users, $user);
        }

        return $users;
    }

    // funding code exists
    public function checkFundingCode($funding_code){

        $funding_code = DB::escapeString($funding_code);

        $sql = "SELECT * 
                FROM grants 
                WHERE funding_code = ('$funding_code') AND is_active = 1";
        $result = DB::query($sql);        
        if(DBResult::numRows($result) != 0){
            return 1;
        }

        return 0;
    }

    public function getValidGrants($case_id){
        
        $case_id = DB::escapeString($case_id);
        $sql = "SELECT * 
                FROM grants 
                WHERE (is_active = 1) 
                AND (grant_problem = (SELECT problem FROM cases WHERE case_id = ('$case_id')) OR grant_problem IS NULL)
                AND (grant_sp_problem = (SELECT sp_problem FROM cases WHERE case_id = ('$case_id')) OR grant_sp_problem IS NULL)
                AND ((county_restriction = 1 AND grant_county = (SELECT county FROM contacts WHERE contact_id = (SELECT client_id FROM cases WHERE case_id = ('$case_id')))) OR grant_county IS NULL)
                AND (gender_restriction = (SELECT gender FROM contacts WHERE contact_id = (SELECT client_id FROM cases WHERE case_id = ('$case_id'))) OR gender_restriction IS NULL)
                AND (veteran_grant = (SELECT veteran_household FROM cases WHERE case_id = ('$case_id')) OR veteran_grant IS NULL)
                AND (g_ethnicity = (SELECT ethnicity FROM contacts WHERE contact_id = (SELECT client_id FROM cases WHERE case_id = ('$case_id'))) OR g_ethnicity IS NULL)
                AND (poverty_restricted <= (SELECT poverty FROM cases WHERE case_id = ('$case_id')) OR poverty_restricted IS NULL)";
        
        $result = DB::query($sql);
        $grants = array();
        while($row = DBResult::fetchArray($result))
        {
            extract($row);
            $grant = array(
                "grant_id" => $grant_id,
                "source" => $source
            );
            array_push($grants, $grant);
        }
        return $grants;

    }
    
    public function getFundingCodes()
    {
        $sql = "SELECT * FROM menu_funding";
        $result = DB::query($sql);
        $funding_codes = array();
        while ($row = DBResult::fetchRow($result))
        {
            extract($row);
            $funding_code = array(
                "value" => $value,
                "label" => $label,
                "menu_order" => $menu_order
            );

            array_push($funding_codes, $funding_code);
        }

        return $funding_codes;
    }

    
    public function getAllGenderValues()
    {
        $sql = "SELECT * FROM menu_gender";
        $result = DB::query($sql);
        $gender_vals = array();
        while ($row = DBResult::fetchArray($result))
        {
            extract($row);
            $gender_val = array(
                "value" => $value,
                "label" => $label,
                "menu_order" => $menu_order
            );

            array_push($gender_vals, $gender_val);
        }

        return $gender_vals;
    }
	
	public function getAllEthnicityValues()
    {
        $sql = "SELECT * FROM menu_ethnicity";
        $result = DB::query($sql);
        $ethnicity_vals = array();
        while ($row = DBResult::fetchArray($result))
        {
            extract($row);
            $ethnicity_val = array(
                "value" => $value,
                "label" => $label,
                "menu_order" => $menu_order
            );

            array_push($ethnicity_vals, $ethnicity_val);
        }

        return $ethnicity_vals;
    }

    public function getTransactions($grant_id)
    {
        $sql = "SELECT      transactions.*, users.first_name, users.last_name,
                            caseworkers.first_name AS c_f_name, caseworkers.last_name AS c_l_name,
                            cases.number, menu_category.label
                FROM        transactions
                INNER JOIN  users ON transactions.user_id = users.user_id
                LEFT JOIN   users AS caseworkers ON transactions.caseworker_id = users.user_id
                LEFT JOIN   cases ON transactions.case_id = cases.case_id
                LEFT JOIN   menu_category ON transactions.menu_category = menu_category.value
                WHERE       grant_id = ('$grant_id')";
        $result = DB::query($sql);
        
        $transactions = array();

        while ($row = DBResult::fetchRow($result))
        {
            if ($row['menu_category'] == NULL && $row['case_id'] == NULL)
            {
                $row['label'] = '(Deposit)';
            }

            $transaction = array(
                "transaction_id"    => $row['transaction_id'],
                "user_id"           => $row['user_id'],
                "user_f_name"       => $row['first_name'],
                "user_l_name"       => $row['last_name'],
                "case_id"           => $row['case_id'],
                "case_number"       => $row['number'],
                "caseworker_id"     => $row['caseworker_id'],
                "c_f_name"          => $row['c_f_name'],
                "c_l_name"          => $row['c_l_name'],
                "date_of_service"   => $row['date_of_service'],
                "activity"          => $row['label'],
                "timestamp"         => $row['timestamp'],
                "time_spent"        => $row['time_spent'],
                "notes"             => $row['notes']
            );

            array_push($transactions, $transaction);
        }

        return $transactions;
    }

    public function getMenu($menu_name)
    {
        $menu_name = DB::escapeString($menu_name);

        $sql = "SELECT * FROM $menu_name";
        $result = DB::query($sql) or trigger_error($sql);

        $menu = array();

        while ($row = DBResult::fetchRow($result))
        {
            $values = array(
                "value" => $row['value'],
                "label" => $row['label'],
                "menu_order" => $row['menu_order']
            );

            array_push($menu, $values);
        }

        return $menu;
    }
    
    public function getClientID($case_id){
        $case_id = DB::escapeString($case_id);
        $sql = "SELECT * FROM contacts WHERE contact_id = (SELECT client_id FROM cases WHERE case_id = ('$case_id'))"; 
        $result = DB::query($sql) or trigger_error('Could not find client ID.');
        $row = DBResult::fetchRow($result);

        return $row;


    }
    

}